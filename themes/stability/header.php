<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stability
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" type="image/gif" sizes="32x32">

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/animate.css">
    

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/aos.css">

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/ionicons.min.css">

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/flaticon.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/custom.css">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site page">


	    <nav id="colorlib-main-nav" role="navigation">
	      <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle active"><i></i></a>
	      <div class="js-fullheight colorlib-table">
	        <div class="container">
	        
	        <div class="colorlib-table-cell js-fullheight">
	          <div class="row no-gutters">
	            <div class="col-md-12 text-right">
		            <?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						) );
					?>
	            </div>
	          </div>
	        </div>
	        
	        </div>
	        
	      </div>
	    </nav>



        <header>
            <div class="container relative">
                <div class="colorlib-navbar-brand">
                  <a class="colorlib-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.svg"></a>
                </div>
                
                <div class="lang-switch">
                    <?php
                        if ( is_active_sidebar( 'custom-header-widget' ) ) : ?>
                            <div id="header-widget-area" class="chw-widget-area widget-area" role="complementary">
                            <?php dynamic_sidebar( 'custom-header-widget' ); ?>
                            </div>
                             
                    <?php endif; ?>
                </div>
                <a href="javascript:void(0)" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
            </div>
         </header>


    
    <div id="content" class="site-content">



