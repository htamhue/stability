<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stability
 */

?>

<div class="project-detail-container rtl">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    	
    	
    	<?php the_content(); ?>
    	
    	
    	<div class="project-nav">
    	    <div class="container">
    	        <div class="row">
    	            <div class="col-md-6">
    	                <?php the_post_navigation(); ?>
    	            </div>
    	            <div class="col-md-6"></div>
    	        </div>
    	    </div>
    	</div>
    	
    	
    </article><!-- #post-<?php the_ID(); ?> -->
</div>
