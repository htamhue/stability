<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stability
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <div class="container">
        <div class="row">
            
            <div class="col-md-6">
                <div class="single-post-content">
                    <?php the_title( '<h2 class="entry-title test"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
                        <p>
                    	<?php echo excerpt(15); ?>
                    	</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="single-post-image">
                    <?php stability_post_thumbnail(); ?>
                </div>
            </div>
        </div>
    </div>

</article><!-- #post-<?php the_ID(); ?> -->
