<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package stability
 */

get_header();
?>


    <div class="single-post-container rtl">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			?>
			
		<div class="container">
	        
	        <div class="row">
	            <div class="col-md-12">
	             <div class="single-post-detail-content">
                    <?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
                 </div>
                <div class="single-post-detail-image">
                    <?php stability_post_thumbnail(); ?>
                </div>
                
                <div class="entry-meta">
    				<?php
    				stability_posted_on();
    				stability_posted_by();
    				?>
    			</div><!-- .entry-meta -->
                
                <?php the_content(); ?>
                
                <?php the_post_navigation(); ?>
                	
               </div>
	        </div>

			</div>

    	<?php	endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
get_footer();
