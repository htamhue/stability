<?php

/* Template Name: Home Page */

get_header(); ?>

<style>

.home-wrap {
    padding-top: 100px;
}

#animation-block {
    height: 100vh;
    position: relative;
    max-width: 1140px;
    margin: 0px auto;
}

.shape {
    position: absolute;
    opacity:0;
}




@-webkit-keyframes mover-1 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-1 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-1 {
    top: 10%;
    left:0;
    -webkit-animation: mover-1 5s forwards;
    animation: mover-1 5s forwards;
    -webkit-animation-delay: 10s;
     animation-delay: 10s;
}


@-webkit-keyframes mover-2 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-2 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-2 {
    left: 40%;
    top: 10%;
    -webkit-animation: mover-2 5s forwards;
    animation: mover-2 5s forwards;
    -webkit-animation-delay: 11s;
     animation-delay: 11s;
}


@-webkit-keyframes mover-3 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-3 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}
.shape-3 {
    top: 0%;
    left: 60%;
    -webkit-animation: mover-3 5s forwards;
    animation: mover-3 5s forwards;
    -webkit-animation-delay: 10.5s;
     animation-delay: 10.5s;
}

@-webkit-keyframes mover-4 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-4 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}
.shape-4 {
    right: 0%;
    top: 10%;
    -webkit-animation: mover-4 5s forwards;
    animation: mover-4 5s forwards;
    -webkit-animation-delay: 9.5s;
     animation-delay: 9.5s;
}



@-webkit-keyframes mover-5 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-5 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-5 {
    top: 40%;
    left: 20%;
    -webkit-animation: mover-5 5s forwards;
    animation: mover-5 5s forwards;
    -webkit-animation-delay: 8s;
     animation-delay: 8s;
}

@-webkit-keyframes mover-6 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-6 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-6 {
    top: 42%;
    left: 55%;
    -webkit-animation: mover-6 5s forwards;
    animation: mover-6 5s forwards;
    -webkit-animation-delay: 8.5s;
     animation-delay: 8.5s;
}

@-webkit-keyframes mover-7 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-7 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-7 {
    right: 12%;
    top: 26%;
    -webkit-animation: mover-7 5s forwards;
    animation: mover-7 5s forwards;
    -webkit-animation-delay: 9s;
     animation-delay: 9s;
}

@-webkit-keyframes mover-8 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-8 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}
.shape-8 {
    right: 0%;
    top: 50%;
    -webkit-animation: mover-8 5s forwards;
    animation: mover-8 5s forwards;
    -webkit-animation-delay: 7.5s;
     animation-delay: 7.5s;
}




@-webkit-keyframes mover-9 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-9 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape.shape-9 {
    left: 0%;
    top: 60%;
    -webkit-animation: mover-9 5s forwards;
    animation: mover-9 5s forwards;
    -webkit-animation-delay: 6.5s;
     animation-delay: 6.5s;
}

@-webkit-keyframes mover-10 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-10 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-10 {
    left: 30%;
    top: 62%;
    -webkit-animation: mover-10 5s forwards;
    animation: mover-10 5s forwards;
    -webkit-animation-delay: 7s;
     animation-delay: 7s;
}

@-webkit-keyframes mover-11 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-11 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-11 {
    left: 55%;
    top: 48%;
    -webkit-animation: mover-11 5s forwards;
    animation: mover-11 5s forwards;
    -webkit-animation-delay: 6s;
     animation-delay: 6s;
}


@-webkit-keyframes mover-12 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-12 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}


.shape-12 {
    left: 2%;
    top: 82%;
    -webkit-animation: mover-12 5s forwards;
    animation: mover-12 5s forwards;
    -webkit-animation-delay: 1.5s;
     animation-delay: 1.5s;
}

@-webkit-keyframes mover-13 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-13 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-13 {
    left: 11%;
    top: 76%;
    -webkit-animation: mover-13 5s forwards;
    animation: mover-13 5s forwards;
    -webkit-animation-delay: 2.5s;
     animation-delay: 2.5s;
}

@-webkit-keyframes mover-14 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-14 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-14 {
    left: 16%;
    top: 78%;
    -webkit-animation: mover-14 5s forwards;
    animation: mover-14 5s forwards;
    -webkit-animation-delay: 3.5s;
     animation-delay: 3.5s;
}

@-webkit-keyframes mover-15 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-15 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}
.shape-15 {
    left: 26%;
    top: 83%;
    -webkit-animation: mover-15 5s forwards;
    animation: mover-15 5s forwards;
    -webkit-animation-delay: 4.5s;
     animation-delay: 4.5s;
}

@-webkit-keyframes mover-16 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-16 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}
.shape-16 {
    left: 43%;
    top: 72%;
    -webkit-animation: mover-16 5s forwards;
    animation: mover-16 5s forwards;
    -webkit-animation-delay: 5.5s;
     animation-delay: 5.5s;
}

@-webkit-keyframes mover-17 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-17 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-17 {
    left: 56%;
    top: 78%;
    -webkit-animation: mover-17 5s forwards;
    animation: mover-17 5s forwards;
    -webkit-animation-delay: 5s;
     animation-delay: 5s;
}

@-webkit-keyframes mover-18 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-18 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

.shape-18 {
    left: 69%;
    top: 77%;
    -webkit-animation: mover-18 5s forwards;
    animation: mover-18 5s forwards;
    -webkit-animation-delay: 4s;
     animation-delay: 4s;
}

@-webkit-keyframes mover-19 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-19 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}
.shape-19 {
    left: 74%;
    top: 88%;
    -webkit-animation: mover-19 5s forwards;
    animation: mover-19 5s forwards;
    -webkit-animation-delay: 3s;
     animation-delay: 3s;
}

@-webkit-keyframes mover-20 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-20 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}
.shape-20 {
    top: 74%;
    right: 8%;
    -webkit-animation: mover-20 5s forwards;
    animation: mover-20 5s forwards;
    -webkit-animation-delay: 2s;
     animation-delay: 2s;
}

@-webkit-keyframes mover-21 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}

@keyframes mover-21 {
    0% { opacity:0; transform: translateY(-150vh); }
    100% { opacity:1; transform: translateY(0); }
}
.shape-21 {
    top: 90%;
    right: 0%;
    -webkit-animation: mover-21 5s forwards;
    animation: mover-21 5s forwards;
    -webkit-animation-delay: 1s;
     animation-delay: 1s;
}


</style>



<section class="home-wrap">
   <a href="/פרוייקטים/">
   <div id="animation-block">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lottie-web/5.6.10/lottie.min.js" type="text/javascript"></script>
<script>
var animation = bodymovin.loadAnimation({
    container: document.getElementById('animation-block'),
    path: 'https://uploads-ssl.webflow.com/5eb4173ae6e68dbfd311e690/5eb4195356068a35fe104b4d_cubes.json',
    renderer: 'svg/canvas/html',
    loop: false,
    autoplay: true
})
</script>

   </div>
   </a>
</section>





<?php get_footer(); ?>


