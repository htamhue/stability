<?php 
/* Template Name: Project */
get_header(); ?>
<style>
::-webkit-scrollbar {
  display: none;
}
/* Modal Content */
.modal {
    overflow-y: scroll;
}
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}
div#myModal {
    background: #000000cf;
    padding: 25px 15px;
}
</style>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<div class="project-container rtl">
    <div class="container">
        
        <div class="row">
            <div class="col-md-12">
                <div class="project-header">
                    <?php
                        if(ICL_LANGUAGE_CODE=='en'){
                          echo '<h2>Projects</h2>';
                        }
                        if(ICL_LANGUAGE_CODE=='he'){
                          echo '<h2>פרוייקטים</h2>';
                        }
                    ?>

                    <div class="project-filter">
                        <span id="show-filter">
                        <?php
                        if(ICL_LANGUAGE_CODE=='en'){
                          echo 'Show all';
                        }
                        if(ICL_LANGUAGE_CODE=='he'){
                          echo 'הצג הכל';
                        }
                    ?>
                        </span>
                        <div class="project-category">
                            <ul class="nav nav-tabs" role="tablist">
                            <?php
                            $categories = get_categories();
                            $count1 = 1;
                                foreach($categories as $category) { ?>
                                   <li class="<?php if($count1 == 1) { echo 'active'; } ?>"><a href="#" data-tab="<?php echo $category->term_id; ?>"><?php echo $category->name ?></a></li>
                              <?php     $cat_slug = $category->slug;
                               $count1++;
                                }
                                
                                ?>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="tab-content">
            <?php 
            $count = 1;
            foreach($categories as $category) { 
            
            ?>
            <div class="tab-pane <?php if($count == 1) { echo 'active'; } ?>" id="<?php echo $category->term_id; ?>">
                <div class="row">
            <?php
                 $the_query = new WP_Query(array(
      'post_type' => 'project',
                  'post_status' => 'publish',
                  'posts_per_page' => -1,
      'category_name' => $category->slug,
      'orderby' => 'date',
            'order'   => 'DESC',
           // 'suppress_filters' => true,
        )); 
     while ( $the_query->have_posts() ) : 
     $the_query->the_post();
          $count++;       
            ?>
            
            <div class="col-md-3">
                <div class="project-grid">
                    <div class="project-image">
                    
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="">
                            <?php stability_post_thumbnail(); ?>
                        </a>
 
                    
                    </div>
                    <?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
                </div>
            </div>
            
             <?php 

                  $i=$i+1;
                  endwhile;

                

            ?>
            
        </div>
            </div>
        
         <?php  } ?>
        </div>
    </div>
</div>

<!--<div id="myModal" class="modal">
  <span class="close cursor close-popup">&times;</span>
  <div class="modal-content">
    OK
  </div>
</div>-->
<script>
jQuery( document ).ready(function() {
    jQuery( "#show-filter" ).click(function() {
        jQuery( ".project-category" ).toggle('slow');
    });
    
    jQuery('a.open-popup').on('click',function(e){
      e.preventDefault();
      var src = jQuery(this).find(".post-thumbnail img").attr('src');
      jQuery("#myModal .modal-content").html('<img src="'+src+'" />');
      jQuery("#myModal").show();
    });
    
    jQuery('#myModal span.close-popup').on('click',function(e){
      jQuery("#myModal").hide();
      jQuery("#myModal .modal-content").html('');
    });
    
    
    jQuery( ".nav-tabs li" ).click(function() {
        jQuery('.nav-tabs li').each(function(){
            jQuery(this).removeClass('active');
        });
        jQuery(this).addClass('active');
        var url = jQuery(this).find('a').attr('data-tab');
        console.log(url);
        jQuery('.tab-pane').each(function(){
            jQuery(this).removeClass('active');
        });
        jQuery('#'+url).addClass('active');
        
    });
});

</script>




<?php  get_footer(); ?>