<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stability
 */

?>


	</div><!-- #content -->
	<style>
::-webkit-scrollbar {
  display: none;
}
/* Modal Content */
.modal {
    overflow-y: scroll;
}
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}
div#myModal {
    background: #000000cf;
    padding: 25px 15px;
}
i.fa.fa-search {
    background: #f5f5f5;
    padding: 10px;
    border-radius: 50%;
    position: absolute;
    top: 10px;
    left: 10px;
    z-index: 9999;
    cursor:pointer;
}
</style>
<div id="myModal" class="modal" data-backdrop="true">
  <span class="close cursor close-popup">&times;</span>
  <div class="modal-content">
    OK
  </div>
</div>
	<footer id="colophon" class="site-footer">
		<div class="footer-nav">
			<div class="container">
				<?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer-menu',
                        'menu_id'        => 'footer-menu',
                    ));
                ?>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>


    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/popper.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.easing.1.3.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.waypoints.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.stellar.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/aos.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.animateNumber.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/scrollax.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/zoom_js.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    
    <script>
  jQuery('.single-project').find('.swiper-slide').each(function(){
        //console.log('slide');
         jQuery(this).find('img').parent().parent().prepend('<i class="fa fa-search"></i>')
        jQuery(this).find('img').wrap('<a href="" class="lightbox open-popup"></a>');
    });
    
    jQuery('.fa-search').click(function(){
        jQuery(this).next().find('a.open-popup').trigger('click');
    });
    var langCode = jQuery('html').attr('lang');
    jQuery('a.open-popup').on('click',function(e){
      e.preventDefault();
      jQuery( ".swiper-slide" ).each(function(){
          jQuery('.swiper-slide').attr('data-nav','');
      });      
      
      jQuery(this).parent().parent().prev().attr('data-nav','mod-prev');
      
      if(jQuery(this).parent().parent().next('.swiper-slide').next('.swiper-slide').length){
          console.log('in if');
          jQuery(this).parent().parent().next().attr('data-nav','mod-next');
      }
      if(jQuery(this).parent().parent().next('.swiper-slide').next('.swiper-slide').length == 0){
          console.log('in else');
          jQuery( ".swiper-slide" ).first().next().attr('data-nav','mod-next');
          
      }
      var src = jQuery(this).find("img").attr('src');
      jQuery("#myModal .modal-content").html('<button class="prev"><i class="eicon-chevron-left" aria-hidden="true"></i></button><div class="img"><img src="'+src+'" /></div><button class="next"><i class="eicon-chevron-right" aria-hidden="true"></i></button>');
      jQuery("#myModal").show();
      
      // if(langCode != 'en-US')
      //   jQuery(".modal-content .img").zoom();
      
       jQuery('.next').on('click',function(e){
         
             jQuery('.swiper-slide[data-nav="mod-next"]').find('a').trigger('click');
      });
      
      
       jQuery('.prev').on('click',function(e){
        //console.log('fdfsdfsdfsdfsdfsdf');
         jQuery('.swiper-slide[data-nav="mod-prev"]:first').find('a').trigger('click');
          jQuery('.swiper-slide').first().find('a').trigger('click');
         
         
      });
      
      
    });
    
   
    
     jQuery('#myModal span.close-popup').on('click',function(e){
      jQuery("#myModal").hide();
      jQuery("#myModal .modal-content").html('');
    });
    jQuery(document).mouseup(function(e) 
    {
        var container = jQuery("#myModal .modal-content");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
          jQuery("#myModal").hide();
        }
    });
    //jQuery('.single-project').find('.swiper-slide').each(function(){
       // jQuery(document).ready(function(){
            
        //});
    //});
	jQuery(document).ready(function(){
		var href = jQuery("header #header-widget-area ul li a").attr('href');
		var text = jQuery("header #header-widget-area ul li a").text();
		if (text == 'Eng') {
			text = 'En';
		}
		jQuery("#colorlib-main-nav ul li:last").addClass('not_border');
		jQuery("header #header-widget-area ul li a span").text(text);
		jQuery('#primary-menu').append('<li class="open_mobile"><a href="'+href+'">'+text+'</a></li>');
	});	
    
  </script>
    

</body>
</html>
